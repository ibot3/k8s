---
- name: Show k8s version listed in playbook
  ansible.builtin.debug:
    msg: |
      "Current k8s version according to the inventory is {{ k8s_version }}.
      Execute playbook with -v to see output of apt-cache policy.
      Pass the version to which the cluster should be updated as parameter 'next_k8s_version',
      i.e., ansible-playbook -i .. 05_upgrade_cluster.yaml -e next_k8s_version=1.15.4.
      Afterwards, update the value of k8s_version in the inventory."

- name: Obtain installed packages
  ansible.builtin.package_facts:
    manager: auto

- name: Pre-flight checks
  any_errors_fatal: true
  block:
  - name: Obtain installed version
    ansible.builtin.set_fact:
      k8s_installed_version: "{{ ansible_facts.packages['kubeadm'][0]['version'].split('-')[0] }}"

  - name: Obtain version as per k8s database
    delegate_to: "{{ (groups['masters'] | sort)[0] }}"
    become: true
    environment:
      KUBECONFIG: /etc/kubernetes/admin.conf
    kubernetes.core.k8s_info:
      api_version: v1
      kind: Node
      name: "{{ inventory_hostname }}"
    register: k8s_node_info

  - name: Extract k8s version
    ansible.builtin.set_fact:
      k8s_running_version: "{{ k8s_node_info.resources[0].status.nodeInfo.kubeletVersion.strip('v') }}"

  - name: Check for failed upgrades
    ansible.builtin.fail:
      msg: |
        Installed version is {{ k8s_installed_version }}, but the running
        version is {{ k8s_running_version }}. This indicates that the upgrade
        of this node was interrupted and you need to fix that manually before
        proceeding.
    when: "k8s_running_version != k8s_installed_version"

  - name: Require installed version to match configuration
    ansible.builtin.fail:
      msg: |
        Installed version is {{ k8s_installed_version }}, which differs from
        the configured version {{ k8s_version }} and the target version {{ next_k8s_version }}.
    when: "k8s_installed_version != k8s_version and k8s_installed_version != next_k8s_version"

  - name: Require upgrade to {{ next_k8s_version }}
    ansible.builtin.fail:
      msg: |
        The target version was set to {{ next_k8s_version | default('') | to_json }}.
        This is not a patch version of {{ next_minor_k8s_version }} You need to set the version via
        the ansible variable next_k8s_version.
    when: "next_k8s_version is not defined or not next_k8s_version.startswith(next_minor_k8s_version)"

  - name: Check if the k8s version {{ next_k8s_version }} is supported
    ansible.builtin.fail:
      msg: |
        Right know the highest supported k8s version is {{ supported_k8s_versions[-1] }}.x
    when: next_minor_k8s_version not in supported_k8s_versions

  - name: Require a iterated update step from {{ supported_k8s_versions[index_present | int] }}.x
    ansible.builtin.fail:
      msg: |
        Right now there is the k8s version {{ supported_k8s_versions[index_present | int] }}.x installed.
        Kubernetes only allows upgrades between minor versions,
        and arbitrary up/downgrades within the patch levels of a minor version.
        It's not possible to update from version {{ k8s_installed_version }} to {{ next_k8s_version }}
    when: not (next_minor_k8s_version == supported_k8s_versions[index_present | int] or
          next_minor_k8s_version == supported_k8s_versions[index_successor | int])

- name: Detect if upgrade is already done
  ansible.builtin.set_fact:
    # we use a boolean here instead of an expression since I’ve seen odd
    # behaviour with non-string valued set_fact statements; I’d like to be sure
    # that there is no false-positive for this flag.
    k8s_upgrade_done: true
  when: "next_k8s_version == k8s_installed_version"

- name: Inform about skipped upgrade
  ansible.builtin.debug:
    msg: |
      Installed version {{ k8s_installed_version }}
      (and running version {{ k8s_running_version }})
      match the expected version {{ next_k8s_version }}, so this node will not
      see any update activity.
  when: "k8s_upgrade_done | default(False)"

- name: Update the package cache
  become: true
  ansible.builtin.apt:
    update_cache: true
  register: task_result
  until: task_result is not failed
  retries: "{{ network_error_retries }}"
  delay: "{{ network_error_delay }}"
  when: ansible_pkg_mgr == 'apt'

- name: Show the available k8s versions
  ansible.builtin.command: apt-cache policy kubeadm
  changed_when: false
  when: ansible_pkg_mgr == 'apt'
...
