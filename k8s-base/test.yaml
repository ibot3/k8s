---
- name: Detect user
  hosts: k8s_nodes
  gather_facts: false
  vars_files:
  - vars/etc.yaml
  roles:
  - detect_user

- name: Check K8s setup
  hosts: k8s_nodes
  become: false
  gather_facts: false
  tasks:
  - name: Get node info
    delegate_to: localhost
    kubernetes.core.k8s_info:
      kind: node
      name: "{{ inventory_hostname }}"
    register: result
    tags:
    - test-ccm

  - name: Check that all hosts are initialized by the cloud provider
    vars:
      taints: "{{ result | json_query('resources[*].spec.taints[*]') }}"
    ansible.builtin.fail:
    when: "'node.cloudprovider.kubernetes.io/uninitialized' in taints"
    tags:
    - test-ccm

  - name: Check that the Internal IP address of each node is set
    vars:
      ip_address: "{{ result | json_query('resources[*].status.addresses[*].address') | count }}"
    ansible.builtin.fail:
    when: "ip_address == '0'"
    tags:
    - test-ccm

- name: Validate DualStack-Support
  hosts: k8s_nodes
  roles:
  - role: check-dualstack
    tags:
    - test-dualstack
    - check-dualstack
    when: dualstack_support

- name: Test Calico CNI Support
  hosts: k8s_nodes
  gather_facts: false
  vars_files:
  - vars/retries.yaml
  roles:
  - role: check-calico
    tags:
    - check-calico
    - test-calico
    - check-dualstack
    - test-dualstack
    when: k8s_network_plugin == 'calico'

- name: Validate IPSec
  hosts: localhost
  gather_facts: false
  vars_files:
  - vars/retries.yaml
  roles:
  - role: check-ipsec
    tags:
    - check-ipsec
    - test-ipsec
    when: ipsec_enabled and ipsec_test_enabled

- name: Test Nvidia GPU Support
  hosts: localhost
  vars_files:
  - vars/retries.yaml
  roles:
  - role: check-nvidia-device-plugin
    when: k8s_is_gpu_cluster and not k8s_virtualize_gpu
    tags:
    - test-nvidia-device-plugin
    - check-nvidia-device-plugin

# Ugly: assuming naming scheme here.
- name: Annotate worker nodes
  hosts: localhost
  become: false
  gather_facts: false
  tags:
  - label
  tasks:
  - name: Annotate the worker nodes  # noqa no-changed-when
    ansible.builtin.command: "kubectl label --overwrite nodes {{ item['worker'] }} name={{ item['name'] }}"
    loop: "{{ test_worker_nodes }}"
    tags:
    - test-services
    - check-services
    - test-block-storage-cinder
    - check-block-storage-cinder
    - test-block-storage-ceph
    - check-block-storage-ceph
    - test-ceph-shared-filesystem
    - check-ceph-shared-filesystem
    - test-shared-fs
    - check-shared-fs

- name: Check services
  hosts: masters[0]
  become: false
  gather_facts: false
  vars_files:
  - vars/retries.yaml
  vars:
    gateway: "{{ groups['gateways'] | first or groups['masters'] | first }}"
    deprecated_nodeport_lb_test_port: "{{ hostvars[groups['frontend'] | first ]['deprecated_nodeport_lb_test_port'] | default(0) }}"
  roles:
  - role: check-services
    when: "deprecated_nodeport_lb_test_port | int > 0"
    tags:
    - test-services
    - check-services
    - test-dualstack
    - check-dualstack
  - role: check-loadbalancer-service
    tags:
    - test-loadbalancer-service
    - check-loadbalancer-service
    - test-dualstack
    - check-dualstack
    when: (openstack_lbaas | default(False)) or ch_k8s_lbaas_enabled

- name: Test enforcement of NetworkPolicies
  hosts: k8s_nodes[-1]
  become: false
  gather_facts: false
  vars_files:
  - vars/retries.yaml
  roles:
  - role: check-networkpolicy-enforcement
    tags:
    - test-networkpolicy-enforcement
    - check-networkpolicy-enforcement
    when: "k8s_network_plugin in ['calico']"

# Same line for argumentation: especially rook is known to take a bit more time to boot up
- name: Check storage services
  hosts: masters[0]
  become: false
  gather_facts: false
  vars_files:
  - vars/retries.yaml
  pre_tasks:
  # include default vars of rook
  - name: include rook defaults
    when: k8s_storage_rook_enabled | bool
    ansible.builtin.include_vars:
      file: "{{ ksl_playbook_directory }}/roles/rook_v1/defaults/main.yaml"
    tags:
    - always
  - name: include ksl inventory variables
    when: ksl_vars_directory is defined
    ansible.builtin.include_vars:
      file: "{{ ksl_vars_directory }}/rook.yaml"
    tags:
    - always
  vars:
    gateway: "{{ groups['gateways'] | first }}"
  roles:
  - role: check-block-storage
    vars:
      block_storage_class: csi-sc-cinderplugin
    when: on_openstack | default(True)
    tags:
    - test-block-storage-cinder
    - check-block-storage-cinder
  - role: check-block-storage
    vars:
      block_storage_class: rook-ceph-data
    when: "k8s_storage_rook_enabled | bool"
    tags:
    - test-block-storage-ceph
    - check-block-storage-ceph
  - role: check-shared-fs
    vars:
      fs_storage_class: rook-ceph-cephfs
    when: "(k8s_storage_rook_enabled | bool) and (rook_ceph_fs | bool)"
    tags:
    - test-ceph-shared-filesystem
    - check-ceph-shared-filesystem
    - test-shared-fs
    - check-shared-fs
  - role: check-local-storage
    vars:
      storageclass: "{{ k8s_local_storage_static_storageclass_name | default('local-storage') }}"
    when: k8s_local_storage_static_enabled
    tags:
    - test-local-storage-static
    - check-local-storage-static
  - role: check-local-storage
    vars:
      storageclass: "{{ k8s_local_storage_dynamic_storageclass_name | default('local-storage') }}"
    when: k8s_local_storage_dynamic_enabled
    tags:
    - test-local-storage-dynamic
    - check-local-storage-dynamic

- name: Remove worker node annotation
  hosts: localhost
  become: false
  gather_facts: false
  tags: label
  tasks:
  - name: Remove annotation of worker nodes  # noqa no-changed-when
    ansible.builtin.command: "kubectl label --overwrite nodes {{ item['worker'] }} name-"
    loop: "{{ test_worker_nodes }}"
    tags:
    - test-services
    - check-services
    - test-block-storage-cinder
    - check-block-storage-cinder
    - test-block-storage-ceph
    - check-block-storage-ceph
    - test-ceph-shared-filesystem
    - check-ceph-shared-filesystem
    - test-shared-fs
    - check-shared-fs
    - test-cleanup
...
