ansible~=7.6.0
kubernetes
openshift
toml
kubernetes-validate
python-openstackclient
hvac

# helper scripts requirements
mergedeep
loguru
pyyaml
packaging
jsonschema
