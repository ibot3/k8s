---
- name: Include version checks and updates
  ansible.builtin.include_tasks: version_checks.yaml

- name: Configure k8s resources
  when: k8s_storage_rook_enabled | bool
  block:
  - name: Check that mon volume storage class exists
    kubernetes.core.k8s_info:
      kind: StorageClass
      name: "{{ rook_mon_volume_storage_class }}"
    when: "rook_mon_volume"
    register: mon_storage_class
    failed_when: "not mon_storage_class.resources"

  - name: Check that osd volume storage class exists
    kubernetes.core.k8s_info:
      kind: StorageClass
      name: "{{ rook_osd_storage_class }}"
    register: osd_storage_class
    when: rook_on_openstack
    failed_when: "not osd_storage_class.resources"

  # Starting with rook v1.5, the CRDs are not in common.yaml anymore
  - name: Create Custom Resource Definitions (CRDs) (rook >= v1.5)
    vars:
      crds_template: "{{ '%s/crds.yaml' | format(rook_conf_maj_min_version) }}"
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', crds_template) }}"
      validate:
        fail_on_error: true
        strict: true
    when: rook_version is version('v1.5.0', operator='ge')
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

  - name: Create common resource definitions
    vars:
      common_template: "{{ '%s/common.yaml' | format(rook_conf_maj_min_version) }}"
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', common_template) }}"
      validate:
        fail_on_error: true
        strict: true
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

  - name: Create rook-monitoring RBAC
    vars:
      mon_rbac_template: "{{ '%s/monitoring-rbac.yaml.j2' | format(rook_conf_maj_min_version) }}"
    when:
    - k8s_monitoring_enabled | bool
    - rook_version is version('v1.5.0', operator='ge')
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', mon_rbac_template) }}"
      validate:
        fail_on_error: true
        strict: true
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

  - name: Create the operator
    vars:
      operator_template: "{{ '%s/operator.yaml' | format(rook_conf_maj_min_version) }}"
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', operator_template) }}"
      validate:
        fail_on_error: true
        strict: true
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

  - name: Create the cluster
    vars:
      cluster_template: "{{ '%s/cluster.yaml' | format(rook_conf_maj_min_version) }}"
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', cluster_template) }}"
      validate:
        fail_on_error: true
        strict: true
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

  - name: Wait for cluster to become ready
    kubernetes.core.k8s_info:
      kind: CephCluster
      namespace: "{{ rook_namespace }}"
      name: "{{ rook_cluster_name }}"
    register: cluster_info
    until: |
      (cluster_info.resources | default(False) and (
        cluster_info.resources[0].status | default(False)
      ) and (
        cluster_info.resources[0].status.ceph | default(False)
      ) and
        (cluster_info.resources[0].status.ceph.health == 'HEALTH_OK' or
         cluster_info.resources[0].status.ceph.health == 'HEALTH_WARN') and
        cluster_info.resources[0].status.state == 'Created')
    delay: 10
    retries: 120

  - name: Wait for the first OSD to appear
    # we need the first OSD otherwise setting up pools will fail
    kubernetes.core.k8s_info:
      kind: Deployment
      namespace: "{{ rook_namespace }}"
      name: "{{ rook_cluster_name }}-osd-0"
    register: osd_info
    until: |
      osd_info.resources | default(False)  and (
        osd_info.resources[0].status | default(False)
      ) and (
        (osd_info.resources[0].status.readyReplicas | default(0)) > 0
      )
    delay: 2
    retries: 60

  - name: Setup toolbox
    vars:
      toolbox_template: "{{ '%s/toolbox.yaml' | format(rook_conf_maj_min_version) }}"
    kubernetes.core.k8s:
      state: "{{ rook_toolbox | ternary('present', 'absent') }}"
      apply: true
      definition: "{{ lookup('template', toolbox_template) }}"
      validate:
        fail_on_error: true
        strict: true
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

  - name: Create ceph pools
    vars:
      pool_template: "{{ '%s/pool.yaml' | format(rook_conf_maj_min_version) }}"
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', pool_template) }}"
      validate:
        fail_on_error: true
        strict: true
    loop: "{{ rook_pools }}"
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

  - name: Create pool storage classes
    vars:
      block_storage_class_template: "{{ '%s/block_storage_class.yaml' | format(rook_conf_maj_min_version) }}"
    kubernetes.core.k8s:
      state: present
      apply: true
      definition: "{{ lookup('template', block_storage_class_template) }}"
      validate:
        fail_on_error: true
        strict: true
    when: "item.create_storage_class | default(False)"
    loop: "{{ rook_pools }}"
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

  - name: Create shared filesystem
    vars:
      fs_template: "{{ '%s/filesystem.yaml' | format(rook_conf_maj_min_version) }}"
    kubernetes.core.k8s:
      state: "{{ rook_ceph_fs | ternary('present', 'absent') }}"
      apply: true
      definition: "{{ lookup('template', fs_template) }}"
      validate:
        fail_on_error: true
        strict: true
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"

  - name: Create shared filesystem storage class
    vars:
      cephfs_storage_class_template: "{{ '%s/cephfs_storage_class.yaml' | format(rook_conf_maj_min_version) }}"
    kubernetes.core.k8s:
      state: "{{ rook_ceph_fs | ternary('present', 'absent') }}"
      apply: true
      definition: "{{ lookup('template', cephfs_storage_class_template) }}"
      validate:
        fail_on_error: true
        strict: true
    # Retry this task on failures
    register: k8s_apply
    until: k8s_apply is not failed
    retries: "{{ k8s_error_retries }}"
    delay: "{{ k8s_error_delay }}"
...
